# Como levantar el proyecto

Una vez instalado Poetry, ejecutar los siguientes comandos:

-   `poetry shell`
-   `export FLASK_APP=app/main.py` (Que es donde está nuestro punto de acceso a la aplicación)
-   `flask --debug run`

Y podras visualizar la aplicación en el navegador en la ruta http://127.0.0.1:5000. Con ctrl + c podrás detener el servidor.
