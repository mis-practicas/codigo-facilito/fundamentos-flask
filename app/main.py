# app.py
from flask import Flask, render_template, request
app = Flask(__name__)

""" if __name__ == '__main__':
    app.run(debug=True, port=5005) """


@app.before_request
def before_request():
    print('Antes de la petición...')


@app.after_request
def after_request(response):
    print('Despues de la petición...')
    return response


@app.route('/')
def index():
    print('Estamos realizando la petición...')
    # return 'Codigo Facilito'
    #  return render_template('index.html', title='Página principal')
    data = {
        'title': 'Página principal',
        'header': 'Bienvenid@',
    }
    return render_template('index.jinja', data=data)


@app.route('/contacto')
def contacto():
    # return 'Codigo Facilito'
    #  return render_template('index.html', title='Página principal')
    data = {
        'title': 'Contacto',
        'header': 'Bienvenid@',
    }
    return render_template('contacto.jinja', data=data)


@app.route('/hola-mundo')
def holamundo():
    return 'Hola Mundo!'


@app.route('/saludo/<name>')
def saludo(name):
    return 'Hola! {0}'.format(name)


@app.route('/suma/<int:valor1>/<int:valor2>')
def suma(valor1, valor2):
    # return "La suma es: " + str(valor1 + valor2)
    return 'La suma es: {0}'.format((valor1 + valor2))


@app.route('/perfil/<name>/<int:age>')
def perfil(name, age):
    return 'Tu nombre es: {0} y tu edad es: {1}'.format(name, age)


@app.route('/lenguajes')
def leguajes():
    data = {
        'title': 'Lenguajes',
        'header': 'Bienvenid@',
        'hay_lenguajes': True,
        'lenguajes': ['PHP', 'Python', 'Java']
    }
    return render_template('lenguajes.jinja', data=data)

#  Query String
#  /datos?valor1=Python&valor2=28


@app.route('/datos')
def datos():
    print(request.args)
    parametro1 = request.args.get('valor1')
    parametro2 = int(request.args.get('valor2'))
    return 'El valor 1 es: {0}, {1}'.format(parametro1, (parametro2 + 15))
